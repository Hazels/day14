package com.example.demo;

import com.example.demo.entity.Todo;
import com.example.demo.repository.TodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;


    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todo_list_when_get_list_given_repo() throws Exception {
        Todo todo1 = buildTodoItem1WithStateFalse();
        Todo todo2 = buildTodoItem2WithStateTrue();
        todoRepository.saveAll(List.of(todo1, todo2));

        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(jsonPath("$[0].done").value(todo1.isDone()))
                .andExpect(jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(jsonPath("$[1].done").value(todo2.isDone()));
    }

    @Test
    void should_return_todo_item_when_get_todo_item_given_id_and_repo() throws Exception {
        Todo todo1 = buildTodoItem1WithStateFalse();
        Todo todo2 = buildTodoItem2WithStateTrue();
        todoRepository.saveAll(List.of(todo1, todo2));

        client.perform(MockMvcRequestBuilders.get("/todos/{id}", todo1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(todo1.isDone()));
    }

    @Test
    void should_edit_todo_item_when_perform_put_given_text_and_id() throws Exception {
        Todo todo1 = buildTodoItem2WithStateTrue();

        todoRepository.save(todo1);
        Todo newTextTodo = new Todo();
        BeanUtils.copyProperties(todo1, newTextTodo);
        newTextTodo.setText("buy a cat");

        client.perform(put("/todos/{id}", todo1.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(newTextTodo)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(newTextTodo.getText()))
                .andExpect(jsonPath("$.done").value(todo1.isDone()));
    }

    @Test
    void should_edit_todo_item_when_perform_put_given_done_and_id() throws Exception {
        Todo todo1 = buildTodoItem2WithStateTrue();

        todoRepository.save(todo1);
        Todo newTextTodo = new Todo();
        BeanUtils.copyProperties(todo1, newTextTodo);
        newTextTodo.setDone(false);

        client.perform(put("/todos/{id}", todo1.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(newTextTodo)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(newTextTodo.isDone()));
    }

    @Test
    void should_return_todo_item_and_save_to_repo_when_perform_post_given_todo_text() throws Exception {
        Todo todo1 = new Todo("I want to read a book");

        todoRepository.save(todo1);

        client.perform(post("/todos", todo1.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todo1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(todo1.isDone()));

    }

    @Test
    void should_del_todo_item_when_perform_del_given_id() throws Exception {
        Todo todo1 = buildTodoItem2WithStateTrue();

        todoRepository.save(todo1);

        client.perform(delete("/todos/{id}", todo1.getId()))
                .andExpect(status().isNoContent());
        //then
        assertNull(todoRepository.findById(todo1.getId()).orElse(null));
    }

    private static Todo buildTodoItem1WithStateFalse() {
        return new Todo("play with my dog", false);
    }

    private static Todo buildTodoItem2WithStateTrue() {
        return new Todo("I want to sleep", true);
    }

}
