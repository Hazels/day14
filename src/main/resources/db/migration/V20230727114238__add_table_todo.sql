drop table if exists `todo`;

CREATE TABLE `todo` (
`id` INT NOT NULL AUTO_INCREMENT,
`text` VARCHAR(45) NULL,
`done` TINYINT NULL,
PRIMARY KEY (`id`));