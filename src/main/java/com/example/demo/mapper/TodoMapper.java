package com.example.demo.mapper;

import com.example.demo.dto.TodoRequest;
import com.example.demo.dto.TodoResponse;
import com.example.demo.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toResponse(Todo todo) {
            TodoResponse todoResponse = new TodoResponse();
            BeanUtils.copyProperties(todo, todoResponse);
            return todoResponse;
    }

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }
}
