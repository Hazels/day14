package com.example.demo.controller;

import com.example.demo.dto.TodoRequest;
import com.example.demo.dto.TodoResponse;
import com.example.demo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<TodoResponse> getAll() {
        return todoService.getAll();
    }

    @GetMapping("/{id}")
    public TodoResponse getById(@PathVariable Integer id) {
        return todoService.getById(id);
    }

    @PutMapping("/{id}")
    public TodoResponse updateById(@PathVariable Integer id, @RequestBody TodoRequest todoRequest) {
        return todoService.updateById(id, todoRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse insertTodoItem(@RequestBody TodoRequest todoRequest) {
        return todoService.insertTodoItem(todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodoItem(@PathVariable Integer id) {
        todoService.deleteItem(id);
    }

}
