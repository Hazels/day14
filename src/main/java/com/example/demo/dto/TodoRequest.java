package com.example.demo.dto;

public class TodoRequest {
    private String text;

    private boolean done;

    public TodoRequest(String text) {
        this.text = text;
    }

    public TodoRequest() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
