package com.example.demo.service;

import com.example.demo.dto.TodoRequest;
import com.example.demo.dto.TodoResponse;
import com.example.demo.entity.Todo;
import com.example.demo.exception.TodoItemNotFoundException;
import com.example.demo.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.demo.mapper.TodoMapper.toEntity;
import static com.example.demo.mapper.TodoMapper.toResponse;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public TodoRepository getTodoRepository() {
        return todoRepository;
    }

    public List<TodoResponse> getAll() {
        List<Todo> todoList = todoRepository.findAll();
        return todoList.stream().map(
                todo -> toResponse(todo)
        ).collect(Collectors.toList());
    }

    public TodoResponse getById(Integer id) {
        Todo todo = todoRepository.findById(id)
                .orElseThrow(TodoItemNotFoundException::new);
        return toResponse(todo);
    }

    public TodoResponse updateById(Integer id, TodoRequest todoRequest) {
        Todo todo1 = todoRepository.findById(id)
                .orElseThrow(TodoItemNotFoundException::new);
        if (todoRequest.getText() != null) {
            todo1.setText(todoRequest.getText());
        }
        if (todo1.isDone() || todoRequest.isDone())
        {
            todo1.setDone(todoRequest.isDone());
        }
        return toResponse(todoRepository.save(todo1));
    }

    public TodoResponse insertTodoItem(TodoRequest todoRequest) {
        todoRequest.setDone(false);
        return toResponse(todoRepository.save(toEntity(todoRequest)));
    }

    public void deleteItem(Integer id) {
        Optional<Todo> todo = todoRepository.findById(id);
        todo.ifPresent(todoRepository::delete);
    }
}
